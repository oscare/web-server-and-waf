#!usr/bin/python
# -*- coding: utf-8 -*-

from optparse import OptionParser
from sys import argv
import socket
import threading
import os
from sys import exit, stderr
import re
import time
from subprocess import Popen, PIPE, CalledProcessError

#------------------CONSTANTES--------
HOST = '0.0.0.0'
C200='HTTP/1.1 200 OK\r\n'
C302='HTTP/1.1 302 Found\r\n'
C400='HTTP/1.1 400 BAD REQUEST\r\nNo se creo correctamente la peticion.\n'
C403='HTTP/1.1 403 FORBIDDEN\r\nLa solicitud fue legal pero el servidor se niega a responderla.\n'
C404='HTTP/1.1 404 NOT FOUND\r\nArchivo no encontrado.\n'
C405='HTTP/1.1 405 METHOD NOT ALLOWED\r\nMetodo no implementado\n'
C500='HTTP/1.1 500 INTERNAL SERVER ERROR.\r\nError ocurrido al ejecutar un script o del propio servidor\n'
CABECERAS = 'Server: MyServer'

#------------------------------------

"""
	Clase para implementar el servidor concurrente
"""
class ThreadedServer(object):
	def __init__(self, host, args, root_directory):
		self.host = host
		self.args= args
		self.port = args.port
		self.rd = root_directory
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.bind((self.host, self.port))

	def listen(self):
		self.sock.listen(5)
		print "\nServidor escuchando en el puerto ",self.port
		while True:
			client, address = self.sock.accept()
			argum=self.args
			client.settimeout(60)
			#Se abre un hilo al que se le asigna la ejecucion de espera a la peticion del cliente
			threading.Thread(target = self.listenToClient,args = (client,address,argum)).start()

	def listenToClient(self, client, address, args):
		size = 1024
		while True:
			#Se escucha a que el cliente envie una peticion al servidor
			request = client.recv(size).decode('utf-8')
			#Se devuelve la respuesta generada
			response = do_request(str(request),address, [self.host, self.port], args, self.rd)
			client.sendall(response.encode('utf-8'))
			#se cierra la conexion
			client.close()
			#se termina el subproceso del hilo
			return False

#---------------------------------------------------------------------
def access_log(client_Addr,date_req,linea,status,tam_resp,args,root_directory):
	"""
	Funcion que realiza la escritura de las entradas permitidas por el waf
	"""
	if(args.bitacoras!=None):
		if re.match(r'^/.*',args.bitacoras):
			ruta_bits=args.bitacoras
		else:
			ruta_bits=root_directory+"/"+args.bitacoras
		try:
			f = open(ruta_bits+'access.log','a')
			f.write(client_Addr[0]+" -- ["+date_req+"] "+linea+" "+status+" "+tam_resp+"\n")
			f.close()
		except Exception as e:
			printError(e,True)
	else:
		print(client_Addr[0]+" -- ["+date_req+"] "+linea+" "+status+" "+tam_resp)
	
	return 
	
def error_log(client_Addr,date_req,pid,tid,filename,args,root_directory):
	"""
	Funcion que muestra los errores al enviar una peticion al servidor 
	"""
	if(args.bitacoras!=None):
		if re.match(r'^/.*',args.bitacoras):
			ruta_bits=args.bitacoras
		else:
			ruta_bits=root_directory+"/"+args.bitacoras
		try:
			f = open(ruta_bits+'error.log','a')
			f.write("["+date_req+"] [core:error] [pid "+pid+":tid "+tid+"] [client "+client_Addr[0]+"] File does not exist: "+filename+"\n")
			f.close()
		except Exception as e:
			printError(e,True)
	else:
		print("["+date_req+"] [core:error] [pid "+pid+":tid "+tid+"] [client "+client_Addr[0]+"] File does not exist: "+filename)

def audit_log(fecha,ip_cliente,regla,descripcion,request,args,root_directory,server_connection):
	"""
	Funcion que escribe las bitacoras de las acciones realizadas
	por el WAF.
	"""
	if(args.audit!=None):
		if re.match(r'^/.*',args.audit):
			ruta_aud=args.audit
		else:
			ruta_aud=root_directory+"/"+args.audit
	else:
		ruta_aud=root_directory+"/audit.log"

	try:
		f = open(ruta_aud,"a")
		f.write("[Timestamp:"+fecha+"] [Client IP: "+ip_cliente[0]+"| Client Port: "+str(ip_cliente[1])+" | Server IP: "+server_connection[0]+" | Server Port: "+str(server_connection[1])+" | Rule:" + regla+" | Description:"+descripcion+" | Request:"+request+"]\n")
		#f.write("[Timestamp:"+fecha+"] [Client IP:"+ip_cliente+" | Server IP:"+server_connection[0]+" | Server Port: "+str(server_connection[1])+" | Rule:" + regla+" | Description:"+descripcion+" | Request:"+request+"]\n")

		f.close()
	except Exception as e:
		printError(e,True)


#---------------------WAF-----------------------------------------

def waf(request,ipclient,root_directory,args,server_connection):
	"""
	Funcion que aplica el funcionamiento del WAF
	request: peticion que sera revisada
	ipclient: ip de donde proviene la peticion
	"""
	if re.match(r'^/.*',args.waf):
		ruta_reglas=args.waf
	else:
		ruta_reglas=root_directory+"/"+args.waf
	try:
		reglas= list()
		f = open(ruta_reglas,"r")
		for line in f:
			reglas.append(line.strip())
		return trata(reglas,request,ipclient,args,root_directory,server_connection)
	
	except Exception:
		printError("El archivo "+ ruta_reglas+ " no existe", True);
		


def revisa_variables(tupla,request,ipclient):
	"""
	Funcion que revisa las variables dentro de cada regla
	y obtiene la parte de la peticion que concuerde con estas
	regres[andolas en forma de lista
	tupla: una regla del archivo de reglas expresada en tupla
	request: peticion que va a ser revisada
	ipclient: ip del cliente
	"""
	resultados=list() #lo que se regresa 
	variables=tupla[1].split("|")#para obtener todas las variables
	parts=request.split("\n")#Lista de lineas del request
	first_line = re.match(r'([A-Z]+) (.*) ([A-Z]+/[1-2]\.[0-2]+)', parts[0]) 
	#Por cada variable expresada en una regla hacemos que empate y obtenemos el recurso
	for var in variables:
		if var.strip()=="AGENTE_USUARIO":
			for elem in parts:
				res= re.match(r'User-Agent: (.*)', elem)
				if res:
					resultados.append(res.group(1).replace("\r",""))
		elif var.strip()=="METODO":
			resultados.append(first_line.group(1).replace("\r",""))
		elif var.strip()=="RECURSO":
			resultados.append(first_line.group(2).replace("\r",""))
		elif var.strip()=="CUERPO":
			resultados.append(parts[parts.index("\r"):][1].replace("\r",""))
		elif var.strip()=="CLIENTE_IP":
			resultados.append(ipclient[0])
		elif var.strip()=="CABECERAS_VALORES":
			resultados.extend([x.split(": ")[1].replace("\r","") for x in parts[1:parts.index("\r")]])
		elif var.strip()=="CABECERAS_NOMBRES":
			resultados.extend([x.split(":")[0].replace("\r","") for x in parts[1:parts.index("\r")]])
		elif var.strip()=="CABECERAS":
			resultados.extend([x.replace("\r","") for x in parts[1:parts.index("\r")]])
		elif var.strip()=="PETICION_LINEA":
			resultados.append(parts[0].replace("\r",""))
		elif var.strip()=="COOKIES":
			new=list()
			for elem in parts:
				res= re.match(r'Cookie: (.*)', elem)
				if res:
					new=res.group(1).split(";")
			for elem in new:
				resultados.append(elem.strip().replace("\r",""))
		else:
			printError("Entrada invalida en el archivo de reglas.\n")
	return resultados
	
def trata(lista,request,ipclient,args,root_directory,server_connection):
	"""
	Funcion que revisa el archivo de reglas y comienza a aplicar
	el filtrado con la peticion que se le pase como parametro.
	lista: el contenido de archivo de reglas
	request: la peticion que se va a revisar
	ipclient: la ip del cliente (Necesaria por los hilos de ejecucion)
	"""
	formato=r'REGLA\->([0-9]+);(.*);(i?regex:\".*\");(.*);(codigo:[0-9]{3}|ignorar)$'
	content=list()
	fecha=time.strftime("%d/%m/%y:%H:%M:%S %Z")
	for reg in lista:
		res=re.match(formato,reg)
		#Si la regla cumple el formato de entrada
		if res:
			content.append((res.group(1),res.group(2),res.group(3),res.group(4),res.group(5)))
		#si no lo cumple manda error y termina la ejecucion
		else:
			printError("Error de Sintaxis: La sintaxis del documento de reglas es incorrecta o contiene errores.",True)
	#Revisamos que no se repita el id de la regla
	for i in range(0,len(content)):
		for j in range(0,len(content)):
			if i!=j and content[j][0]==content[i][0] :
				printError("Se ha repetido el id.\n", True)
	expresado=list()
	#Obtenemos el contenido del request con respecto a las variables de cada regla
	for tupla in content:
		r=revisa_variables(tupla,request,ipclient)
		expresado.append(r)
	for tupla in content:
		aux=tupla[2].split(":")
		tipo=aux[0]
		expresion=aux[1]
		aux=tupla[4].split(":")
		accion=""
		if len(aux)>1:
			accion=aux[1]
		else:
			accion=aux[0]
		resultado=filtro(expresion,tipo,expresado,accion)
		if resultado != "X":
			audit_log(fecha,ipclient,tupla[0],tupla[3],request,args,root_directory,server_connection)
			return resultado
	return "X"
	
	
def filtro(expresion,tipo,contenido, accion):
	"""
	Funcion que aplica la busqueda de la expresion regular en el archivo reglas
	dentro del contenido de las variables en las reglas.
	expresion: expresion regular a buscar
	tipo: si es iregex o regex
	contenido: lista de cadenas donde va a buscar
	accion: la accion que se pretende aplicar
	"""
	for elem in contenido:
		for i in elem:
			if tipo =="iregex":
				res=re.search(expresion.replace("\"",""),i,re.I)
			elif tipo =="regex":
				res=re.search(expresion.replace("\"",""),i)
			else:
				printError("Entrada invalida en el archivo de reglas.\n")
			if res:
				if accion=="ignorar":
					return ""
				else:
					return accion
	return "X"
	
	

#-----------------------------------------------------------------------------------------

def addOptions():
	"""
		Funcion que nos permite dar argumentos al script al momento 
		de su ejecucion. Las configuraciones iniciales que va a tener
		el servidor
	"""
	parser = OptionParser()
	parser.add_option('-p', '--port', type=int,
		dest='port', default=8080, 
		help='Define the port where the server will listen to conections.'
	)
	parser.add_option('-d', '--directory',
		dest='dir', default='.',
		help='Directory that will be used like root directory by the server.'
	)
	parser.add_option('-e', '--debug', 
		dest="debug",
		action="store_const", const=True, 
		help='Print to stdin mode verbose of the server.'
	)
	parser.add_option("-b", "--binnacles",
		dest="bitacoras", help="Directory where binnacles will be stored.",
		default=None)
	parser.add_option("-w", "--waf",
		dest="waf", help="Enable Firewall.", default=None)
	parser.add_option("-a", "--audit",
		dest="audit", help="File of the Firewall's binnacle.",
		default=None)

	options, args = parser.parse_args()
	return options

def printError(msg, ex=False):
	"""
	Funcion que muestra mensaje de error y cierra la ejecucion
	"""
	stderr.write("Error:\t%s\n" % msg)
	if ex:
		exit(1)  

def CGI_request(req_meth, UsAg, dir2, filename, HTTP_protocol, server_connection, client_connection, params):
	"""
		Funcion que realiza en un subproceso la ejecucion del cgi solicitado
		Asigna las variables de entorno requeridas y devuelve la salida del script.
		En caso que el script termine en error, lanza una excepcion NameError (elegida al azar)
	"""
	#Se copian las variables de entorno del SO
	en_var = os.environ.copy()
	print (req_meth, UsAg, dir2, filename, HTTP_protocol, server_connection, client_connection, params)
	if params:
		en_var['QUERY_STRING'] = params
	else:
		en_var['QUERY_STRING'] = ''
	en_var['DOCUMENT_ROOT'] = dir2
	en_var['HTTP_COOKIE'] = "dGhpc2lzbm90YXNhZmVjb29raWU="
	en_var['HTTP_REFERER'] = "referer"
	en_var['HTTP_USER_AGENT'] = UsAg.split(": ")[1]
	en_var['REQUEST_URI'] = filename
	en_var['REMOTE_ADDR'] = client_connection[0]
	en_var['REMOTE_PORT'] = str(client_connection[1])
	en_var['SERVER_SOFTWARE'] = "Python 2.7"
	en_var['SERVER_NAME'] = "MyServer"
	en_var['SERVER_PROTOCOL'] = HTTP_protocol
	en_var['SERVER_PORT'] = str(server_connection[1])
	en_var['REQUEST_METHOD'] = req_meth
	os.environ.update(en_var)

	#En esta parte se analiza el archivo para saber que interprete invocar
	with open(dir2) as cgi_file:
		first_line = cgi_file.readline().strip()
		if (first_line == '#!/usr/bin/python'):
			interprete = "python"
		elif(first_line == '#!/usr/bin/perl'):
			interprete = "perl"
		elif(first_line == '#!/usr/bin/php'):
			interprete = "php"

	print (interprete, filename, params)
	#Se realiza la ejecucion del script en un subproceso
	sub_stdout, sub_stderr = Popen([interprete,dir2],  stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate()
	print (filename, sub_stdout, sub_stderr)
	#Si se obtiene algo en sub_stderr, entonces hubo error en la ejecucion del CGI
	if len(sub_stderr) >= 1:
		print "error del CGI"
		#Se lanza una excepcion para atraparla en el metodo que invoca esta funcion
		#La excepcion fue tomada al azar
		raise NameError("Error en el archivo CGI") 
	else:
		#Si el script termina normal, devuelve su salida
		return sub_stdout

def GET_request(root_directory, resource, request, HTTP_protocol, server_connection, client_connection):
	"""
		Funcion implementada para resolver peticiones unicamente del metodo GET
		Devuelve el contenido del recurso solicitado o devuelve los errores HTTP debidos
		en caso de tener un error con ese recurso
	"""
	response = ''
	print request
	line=re.match('(.*)\?(.*)',resource)
	#Si hace match, mando parametros al recurso solicitado
	if line :
		filename=line.group(1)[1:]#nombre del archivo
		params = str(line.group(2))
	#Si no hace match, solo mando el recurso
	else:
		#Redireccion a index.html
		if resource[1:] == "":
			filename = "index.html"
		else:
			filename = resource[1:]
		#No mando parametros al recurso
		params = None

	#Si no se tiene acceso de lectura, devuelve un error 403
	if (os.path.exists(filename) and not(os.access(filename, os.R_OK))):
		return C403 + CABECERAS + "\n\n"

	try:
		#Si se intenta ejecutar un archivo CGI
		is_cgi = re.match('.*\.(py|pl|php|cgi)$', filename)
		if is_cgi:
			content = CGI_request("GET", request[1], root_directory + "/" + filename,
				 filename, HTTP_protocol, server_connection, 
				 client_connection, params)
		else:
			#si es un archivo normal, se devuelve su contenido
			with open(str(filename)) as f_resource:
				content = f_resource.read()
		response = C200+ CABECERAS + "\n\n" +content
	#esta excepcion es debido a que el recurso no existe
	except IOError:
		response = C404 + CABECERAS + "\n\n"
	#Si se tiene esta excepcion, hubo error en la ejecucion del CGI
	except NameError:
		print "Error al ejecutar el CGI"
		response = C500 + CABECERAS + "\n\n"
	finally:
		return response

def HEAD_request(root_directory, resource, request):
	"""
		Funcion que resuelve unicamente peticiones hechas con HEAD
	"""
	line=re.match('(.*)\?(.*)',resource)
	if line :
		filename=line.group(1)[1:]#nombre del archivo
	#Si no hace match, solo mando el recurso
	else:
		#Redireccion a index.html
		if resource[1:] == "":
			filename = "index.html"
		else:
			filename = resource[1:]

	if os.path.exists(filename):
		#Si no se tiene permisos, devuelve 403
		if (not os.access(filename, os.R_OK)):
			return C403 + CABECERAS + "\n\n"
		#Si existe el archivo devuelve 200, no se lee el contenido
		else:
			response = C200 +CABECERAS+"\n\n"
	#Si no existe devuelve 404
	else:
		response = C404 + CABECERAS + "\n\n"
	
	return response

def POST_request(root_directory, resource, request, HTTP_protocol, server_connection, client_connection):
	"""
		Funcion que resuelve unicamente peticiones hechas por el metodo POST
		Devuelve los response HTTP adecuados de acuerdo al recurso solicitado
	"""
	response = ''
	params = request[-1].strip()
	#Si no se tienen paramtros, no se ejecuta nada
	if params != ['']:
		filename=str(resource[1:]).strip() #nombre del directorio y archivo /index.html
		print filename
		#Si no se tiene permisos, devuelve 403
		if (os.path.exists(filename) and not(os.access(filename, os.R_OK))):
			return C403 + CABECERAS + "\n\n"

		try:
			#Analiza si el recurso solicitado es un script cgi
			is_cgi = re.match('.*\.(py|pl|php|cgi)$', filename)
			if is_cgi:
				content = CGI_request("POST", request[1], root_directory + "/" + filename,
					 filename, HTTP_protocol, server_connection, 
					 client_connection, params)
				print "Si paso=============="
			else:
				#en caso de no serlo, lee el contenido y lo devuelve
				with open(str(filename)) as f_resource:
					content = f_resource.read()
			response = C200 + CABECERAS + "\n\n" + content
		#Error generado si el recurso no existe en el directorio especificado
		except IOError:
			print "no existe"
			response = C404 + CABECERAS + "\n\n"
		#Si se tiene esta excepcion, hubo error en la ejecucion del CGI
		except NameError:
			print "Error al ejecutar el CGI"
			response = C500 + CABECERAS + "\n\n"
		finally:
			return response

def do_request(request,ipclient,server_connection, args, root_directory):
	"""
		Funcion principal del servidor es la que maneja todo. 
		MAneja las peticiones de los clientes,
		devuelve la respuesta al cliente
		utiliza el WAF (en caso de estar habilitado)
		Hace el registro de las bitacoras (en caso de estar habilitado)
	"""
	if args.waf!=None:
		#Si el WAF esta habilitado, entonces se analizan las peticiones
		resWAF=waf(request,ipclient,root_directory,args,server_connection)
		if waf!="X":
			if resWAF=="500":
				return C500+ CABECERAS + "\n\n"
			if resWAF=="403":
				return C403+ CABECERAS + "\n\n"
			if resWAF=="404":
				return C404+ CABECERAS + "\n\n"
			if resWAF=="ignorar":
				return ""
	date_req=time.strftime("%d/%m/%y:%H:%M:%S %Z")#Fecha en que se hace la peticion
	parts = request.split('\n')
	print (request)
	#Metodo recurso y protocolo del request
	first_line = re.match('^([A-Z]{1,6}) (.*) ([A-Z]+/[1-2]\.[0-2])', parts[0])
	if first_line.group(1) == 'GET':
		#mandamos el recurso, el resto de la peticion, el protocolo, datos del servidor y del cliente
		response = GET_request(root_directory, first_line.group(2), parts[1:], first_line.group(3), server_connection, ipclient)
	elif first_line.group(1) == 'POST':
		#mandamos el recurso, el resto de la peticion, el protocolo, datos del servidor y del cliente
		response = POST_request(root_directory, first_line.group(2), parts[1:], first_line.group(3), server_connection, ipclient)
	elif first_line.group(1) == 'HEAD':
		#mandamos el recurso y el resto de la petcion
		response = HEAD_request(root_directory, first_line.group(2), parts[1:])
	else:
		#Error si se solicito un metodo no definido
		response = C405 + CABECERAS + "\n\n"
	filename = first_line.group(2)
	rest=response.split(" ")
	print response
	if rest[1]!="400" and rest[1]!="404": 
		access_log(ipclient,date_req,parts[0].strip(),rest[1],str(len(response)),args,root_directory)
	else:
		error_log(ipclient,date_req,str(os.getpid())," ",filename,args,root_directory)
	
	#devolucion de la respuesta al cliente conectado
	return response

def main():
	try:
		args = addOptions()
		root_directory=os.path.abspath(args.dir)
		#Cambiados de directorio al especificado con -d
		os.chdir(root_directory)
		print ("Directorio: %s\n" % root_directory)
		files=[f for f in os.listdir(".") if os.path.isfile(f)] #contiene los archivos del directorio actual
		print "Archivos en el servidor: %s" %files

		#inicializacion del servidor concurrente
		ThreadedServer(HOST,args, root_directory).listen()
		

	except ValueError as ve:
		print (ve)
		exit(1)

	except socket.error as se:
		printError("El puerto {} ya esta en uso" .format(args.port))

	except OSError as ose:
		printError("El directorio no existe")

	except OverflowError as ofe:
		printError("El puerto elegido debe estar dentro del rango [1..65535]")

	#except Exception:
	#	printError("El directorio que se especifico es incorrecto\n", True)


if __name__ == "__main__":
    main()
